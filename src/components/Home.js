import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const Home = () => {
	//Get the editors from redux store
	const editors = useSelector((state) => state.editorReducer.editors);

	//Prepare the letters to be displayed
	const loadEditors = editors.map((editor, index) => {
		return (
			<div
				className='editor'
				style={{
					width: '60%',
					boxShadow: '1px 1px 7px rgb(41, 40, 40)',
					borderRadius: '10px',
					marginBottom: '20px',
					marginLeft: '20%',
				}}
				key={editor.id}
			>
				<h3 style={{ color: 'indigo' }}>Letter {index + 1}</h3>
				<div
					dangerouslySetInnerHTML={{ __html: editor.editor }}
					style={{
						border: '1px solid black',
						boxShadow: '0px 0px 5px gray',
						margin: '20px',
						padding: '5px',
						borderRadius: '5px',
					}}
				/>
			</div>
		);
	});

	//Render
	return (
		<div>
			<div className='title'>Home</div>
			<Link to='/editors'>Go to Editor</Link>
			<div>{loadEditors}</div>
		</div>
	);
};

export default Home;
