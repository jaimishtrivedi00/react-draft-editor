import React from 'react';
import { useState, useEffect } from 'react';
import EditorItem from './EditorItem';
import { Link } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { addEditor, getEditors } from '../store/actions/editorActions';

const Editors = () => {
	//Local editors and reload state
	const [editors, setEditors] = useState();
	const [reload, setReload] = useState(false);
	//Prepare dispatch
	const dispatch = useDispatch();
	//Get the state editors, from redux state
	const stateEditors = useSelector((state) => state.editorReducer.editors);

	//Hook to set the editors
	useEffect(() => {
		getEditorsFromState();
	}, [reload]);

	//Set the editors
	const getEditorsFromState = () => {
		//Update the redux state editors by fetching from DB
		dispatch(getEditors());

		//Prepare local state editors
		const temp = [];
		stateEditors.forEach((editor) => {
			if (editor) {
				temp.push(
					<EditorItem
						key={editor.id}
						id={editor.id}
						content={editor.editor}
						reload={reload}
						setReload={setReload}
					/>
				);
			}
		});
		//Set local state editors
		setEditors(temp);
	};

	//Render
	return (
		<div>
			<div className='title'>Editors</div>
			{/* Add editor */}
			<button
				onClick={() => {
					setReload(!reload);
					dispatch(addEditor());
				}}
			>
				Add Editor
			</button>
			<br />
			{/* Refresh the editors by fetching from DB */}
			<button onClick={getEditorsFromState}>Refresh</button> Click this if you
			encounter visibility issue with editors.
			<br />
			<Link to='/'>Go to Home</Link>
			{/* Render the Editors */}
			<div className='container'>{editors}</div>
			<br />
		</div>
	);
};

export default Editors;
