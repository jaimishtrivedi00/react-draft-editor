import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import Home from './Home';
import Editors from './Editors';

const Routes = () => {
  return (
    // Router as a root element
    // Renders component based on the current path
    <Router>
      <Switch>
        <Route path="/" exact component={Home}></Route>
        <Route path="/editors" exact component={Editors}></Route>  
      </Switch> 
    </Router>
  )
};

export default Routes;
