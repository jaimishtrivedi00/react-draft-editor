import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { updateEditor, deleteEditor } from '../store/actions/editorActions';

import { Editor } from 'react-draft-wysiwyg';
import '../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { EditorState, convertToRaw, ContentState } from 'draft-js';

const EditorItem = ({
	id,
	content,
	reload = undefined,
	setReload = (f) => f,
}) => {
	//Prepare the initial state of editor item
	let initialState;
	const contentBlock = htmlToDraft(content);
	if (contentBlock) {
		const contentState = ContentState.createFromBlockArray(
			contentBlock.contentBlocks
		);
		initialState = EditorState.createWithContent(contentState);
	}

	if (!initialState) initialState = EditorState.createEmpty();

	//Local states
	const [editor, setEditor] = useState(initialState); //Editor state from Draft.js
	const [show, setShow] = useState(true);

	//Method to change the editor state and enable or disable the save button
	const editorStateChange = (editorState) => {
		const curr = draftToHtml(convertToRaw(editorState.getCurrentContent()));

		if (curr === content) setShow(false);
		else setShow(true);

		setEditor(editorState);
	};

	//Get the HTML content from the editor state
	const getHTMLContent = (editorState) => {
		return draftToHtml(convertToRaw(editorState.getCurrentContent()));
	};

	//Prepare dispatch
	const dispatch = useDispatch();

	//Render
	return (
		<div className='editor'>
			{/* Editor from draft.js */}
			<Editor
				initialEditorState={initialState}
				editorState={editor}
				wrapperClassName='editor-wrapper'
				onEditorStateChange={editorStateChange}
			/>

			<div className='buttons'>
				{/* Save button */}
				<div
					onClick={() => {
						//Get the HTML content to save in the database
						const curr = getHTMLContent(editor);
						//Update editor
						dispatch(updateEditor({ id, curr }));
						setShow(false);
					}}
					className={show ? 'save-btn' : 'disabled-btn'}
				>
					Save the letter
				</div>
				{/* Delete button */}
				<div
					onClick={() => {
						//Update editor
						setReload(!reload);
						dispatch(deleteEditor(id));
					}}
					className='save-btn'
				>
					Delete the letter
				</div>
			</div>
		</div>
	);
};

export default EditorItem;
