const defaultStore = {
	editors: [],
};

const editorReducer = (state = defaultStore, action) => {
	switch (action.type) {
		// xxxxxxxxxxxxxxxxxxxxxxxxxx UPDATE THE EDITOR IN DB xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		case 'UPDATE_EDITOR':
			//Update the database
			sendPayload(action)
				.then((d) => {
					//Update the redux state
					const idx = state.editors.findIndex((e) => e.id === d.id);
					state.editors[idx].editor = d.editor;
				})
				.catch((err) => console.error(err));

			//Return the updated state
			return state;

		// xxxxxxxxxxxxxxxxxxxxxxxxxx ADD THE EDITOR IN DB xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		case 'ADD_EDITOR':
			//Add the editor to database and prepare payload to store in redux state
			let payload;
			sendPayload({}, 'POST')
				.then(
					(d) =>
						(payload = {
							editor: '',
							id: d.name,
						})
				)
				.catch((err) => console.error(err));

			//Return the added editor plus the previous editors
			return {
				editors: [payload, ...state.editors],
			};

		// xxxxxxxxxxxxxxxxxxxxxxxxxx GET THE EDITOR FROM DB xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		case 'GET_EDITORS':
			//Get the editors from DB
			const items = [];
			getPayload()
				.then((d) => {
					for (let item in d) {
						//Prepare the editors for redux state
						items.push({
							editor: d[item].editor,
							id: item,
						});
					}
				})
				.catch((err) => console.error(err));

			//Assign the prepared items to redux state
			return {
				editors: items,
			};

		// xxxxxxxxxxxxxxxxxxxxxxxxxx DELETE CASE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		case 'DELETE_EDITOR':
			sendPayload(action, 'DELETE')
				.then((d) => {
					console.log(d);
					const idx = state.editors.findIndex(
						(e) => e.id === action.payload
					);
					state.editors.splice(idx, 1);
				})
				.catch();

			return state;
		// xxxxxxxxxxxxxxxxxxxxxxxxxx DEFAULT CASE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		default:
			return state;
	}
};

//Helper method to PUT or POST or DELETE The data to DB
const sendPayload = (action, method = 'PUT') => {
	if (method === 'PUT') {
		const id = action.payload;

		//Update the existing editor with 'id' of DB
		return fetch(
			`https://react-draft-editor-default-rtdb.firebaseio.com/editors/${id}.json`,
			{
				method: 'PUT',
				body: JSON.stringify(action.payload),
			}
		)
			.then((response) => {
				return response.json();
			})
			.catch((err) => console.error(err));
	} else if (method === 'POST') {
		//Add the newly created editor to DB
		return fetch(
			`https://react-draft-editor-default-rtdb.firebaseio.com/editors.json`,
			{
				method: 'POST',
				body: JSON.stringify({ editor: '' }),
			}
		)
			.then((response) => {
				return response.json();
			})
			.catch((err) => console.error(err));
	} else if (method === 'DELETE') {
		//Delete the editor from DB
		const id = action.payload;
		return fetch(
			`https://react-draft-editor-default-rtdb.firebaseio.com/editors/${id}.json`,
			{
				method: 'DELETE',
			}
		)
			.then((response) => {
				return response.json();
			})
			.catch((err) => console.error(err));
	}
};

//Helper method to fetch the existing editors from DB
const getPayload = () => {
	return fetch(
		`https://react-draft-editor-default-rtdb.firebaseio.com/editors.json`
	)
		.then((response) => {
			return response.json();
		})
		.catch((err) => console.error(err));
};

//Export the Reducer
export default editorReducer;
