//Update action
const updateEditor = (data) => {
	return {
		type: 'UPDATE_EDITOR',
		payload: {
			editor: data.curr,
			id: data.id,
		},
	};
};

//Add action
const addEditor = () => {
	return {
		type: 'ADD_EDITOR',
	};
};

//Get action
const getEditors = () => {
	return {
		type: 'GET_EDITORS',
	};
};

const deleteEditor = (id) => {
  return {
    type: 'DELETE_EDITOR',
    payload: id,
  }
}

//Export the actions
export { updateEditor, addEditor, getEditors, deleteEditor };
