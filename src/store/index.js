import { createStore, combineReducers } from 'redux';
import editorReducer from './reducers/editor';

//Create store using Reducers
const store = createStore(
	combineReducers({
		editorReducer,
	})
);

export default store;
