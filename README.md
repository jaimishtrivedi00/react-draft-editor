### React Draft Editor

This is the project to demonstrate the use of Draft.js Editor with React.js <br />
Project has the main functionality where, a user can create multiple Editors to edit the letters and also delete them.

### Editor page
<a href="https://ibb.co/WBc11NK"><img src="https://i.ibb.co/zPSwwWf/1.jpg" alt="Editor" border="0"></a>

### Home page
<a href="https://ibb.co/B6DvJPW"><img src="https://i.ibb.co/Wz97X38/2.jpg" alt="Home" border="0"></a><br />

***There is some issue with force mounting the react component. So please use Refresh button whenever you want to Add or Delete the editors***
